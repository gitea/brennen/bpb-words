bpb-words
=========

A command-line utility to split input into a list of words, one per line.

I'm writing this as a learning exercise, based on
[bpb-kit/home/bin/words][words].

**Work in progress, barely functional at present.**

current state
-------------

```sh
# Operate on standard input:
echo 'one two three' | bpb-words -

# ...or just:
echo "one two three" | bpb-words

# Operate on some file:
bpb-words ./foo
```

desired features
----------------

```sh
# list all unique words, ignoring case, in foo:
bpb-words -ui ./foo

# find ten most used words longer than 6 letters in foo:
bpb-words -uci -s6 foo | sort -nr | head -10
```

installing
----------

???

[words]: https://code.p1k3.com/gitea/brennen/bpb-kit/src/branch/master/home/bin/words
