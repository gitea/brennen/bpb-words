use exitfailure::ExitFailure;
use regex::{Regex, Replacer};

// I have idea what is happening here.
// https://docs.rs/regex/1.3.1/regex/trait.Replacer.html
pub fn replace_nonalpha<R: Replacer>(
    src: &str,
    mut rep: R,
) -> String {
    let nonalpha = Regex::new(r"[^[:alpha:]]+").unwrap();
    let dst = nonalpha.replace_all(src, rep.by_ref());
    let dst = nonalpha.replace_all(&dst, rep.by_ref());
    dst.into_owned()
}
